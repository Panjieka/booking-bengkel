package com.bengkel.booking.services;

import com.bengkel.booking.models.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class BengkelService {
	
	private static Customer authenticatedCustomer;
	
	//Login
    public static boolean loginAuth(List<Customer> customerList, Scanner input){
        boolean isLogin = true;
        int attemp = 0;
        while (attemp < 3){
            System.out.println("Masukkan Customer Id : ");
            String customerId = input.nextLine();
            try {
                authenticatedCustomer = Validation.authenticateID(customerList, customerId);
                System.out.println("Masukkan Password : ");
                String password = input.nextLine();
                if (Validation.checkPassword(authenticatedCustomer,password)) {
                    System.out.println("Berhasil login");
                    return isLogin;
                } else {
                    attemp++;
                    if(attemp < 3){
                        System.out.println("Password salah. Silakan Ulangi!");
                    } else {
                        System.out.println("Anda sudah gagal login tiga kali. Program akan berhenti secara otomatis.");
                        System.exit(0);
                    }
                }
            } catch (Exception e){
                attemp++;
                if (attemp < 3){
                    System.out.println("Id Salah. Silakan Ulangi!");
                } else {
                    System.out.println("Anda sudah gagal login tiga kali. Program akan berhenti secara otomatis.");
                    System.exit(0);
                }
            }
        }
        return isLogin;
    }
	
	//Info Customer
    public static void showInfoCustomer (List<Customer> customerList){
        if (authenticatedCustomer != null) {
            PrintService.printCustomer(customerList, authenticatedCustomer.getCustomerId());
            System.out.println();
        } else {
            System.out.println("Silakan login terlebih dahulu.");
        }
    }
    public static void showVehicleCustomer (List<Customer> customerList){
        List<Vehicle> vehicles = Validation.getVehiclesByCustomerId(customerList, authenticatedCustomer.getCustomerId());

        if (vehicles != null) {
                PrintService.printVechicle(vehicles);
        } else {
            System.out.println("Customer tidak ditemukan");
        }
    }
    public static void showBookingOrder (List<BookingOrder> bookingOrderList, List<Customer> customerList){
        Optional<Customer> customers = Validation.getCustumerById(customerList, authenticatedCustomer.getCustomerId());

        PrintService.printAllBookingOrders(bookingOrderList, customers);
    }
	
	//Booking atau Reservation
    public static void createReservation(Scanner input,List<Customer> customerList, List<ItemService> listItemService, List<BookingOrder> listBookingOrder){
        Vehicle choosenVehicle = null;
        Customer customer = null;
        List<Vehicle> vehicles = Validation.getVehiclesByCustomerId(customerList, authenticatedCustomer.getCustomerId());
        List<ItemService> orderedItemServices = new ArrayList<>();
        int maxNumberOfService = authenticatedCustomer.getMaxNumberOfService();
        String paymentMethod = "cash";

        PrintService.printVechicle(vehicles);
        while(true){
            System.out.println("Masukkkan id kendaraan: ");
            String vehicleId = input.nextLine();

            try {
                choosenVehicle = Validation.getVehicleByVehicleId(authenticatedCustomer, vehicleId);
                break;
            } catch (Exception e) {
                System.out.println("Kendaraan tidak ditemukan. Silakan masukan id kendaraan yang benar!");
            }
        }

        PrintService.PrintItemServices(listItemService, choosenVehicle);

        int jumlahService = 1;
        while (jumlahService <= maxNumberOfService) {
            System.out.println("Masukkan id service: ");
            String itemServiceId = input.nextLine();

            try {
                if (Validation.checkAddedItemService(orderedItemServices, itemServiceId)) {
                    System.out.println("Service sudah ditambahkan. Silakan tambahkan service yang lain");
                } else {
                    ItemService choosenItemService = Validation.findItemServiceByItemService(listItemService, itemServiceId);
                    orderedItemServices.add(choosenItemService);

                    if (jumlahService < maxNumberOfService) {
                        String userInput = Validation.validasiInput("Apakah Anda ingin menambahkan service yang lain? (ya (y) /tidak (t)) ", "Hanya bisa menerima input berupa huruf y dan n", "\\b[YyTt]\\b");

                        if (userInput.equalsIgnoreCase("t")) {
                            break;
                        }
                    }

                    jumlahService++;
                }
            } catch (Exception e) {
                System.out.println("Service tidak tersedia. Silakan masukkan service id yang benar.");
            }
        }
        double totalServicePrice = orderedItemServices.stream()
                .reduce(0.0, (subtot, orderedItemService) -> subtot + orderedItemService.getPrice(), Double::sum);

        BookingOrder newBookingOrder = null;

        Customer customers = Validation.validateCustomerId(authenticatedCustomer.getCustomerId(),customerList);
        if(Validation.isMember(customerList,authenticatedCustomer.getCustomerId())){
            MemberCustomer memberCustomer = Validation.getMemberById(customerList,authenticatedCustomer.getCustomerId());
            String choosenPaymentMethod = Validation.validasiInput("Silakan pilih metode pembayaran (saldo coin/cash, tulis dengan huruf kecil semua) ", "Hanya menerima input \"cash\" dan \"saldo coin\" ", "^(cash|saldo coin)$");

            if(choosenPaymentMethod.equalsIgnoreCase("saldo coin")){
                double sisaSaldoCoin = memberCustomer.getSaldoCoin();

                if(sisaSaldoCoin >= totalServicePrice){
                    paymentMethod = choosenPaymentMethod;
                    System.out.println("Berhasil membayar dengan saldo coin!");
                    newBookingOrder = new BookingOrder(generateBookingOrderId(listBookingOrder), customers, orderedItemServices, paymentMethod, totalServicePrice);
                    listBookingOrder.add(newBookingOrder);

                    double newSaldoCoin = memberCustomer.getSaldoCoin() - newBookingOrder.getTotalPayment();
                    memberCustomer.setSaldoCoin(newSaldoCoin);

                } else {
                    System.out.println("Saldo coin tidak cukup! pembayaran secara otomatis dialihkan ke cash");
                    validateCashAmount(totalServicePrice, input);
                    newBookingOrder = new BookingOrder(generateBookingOrderId(listBookingOrder), customers, orderedItemServices, paymentMethod, totalServicePrice);
                    listBookingOrder.add(newBookingOrder);
                }


            } else {
                validateCashAmount(totalServicePrice, input);
                newBookingOrder = new BookingOrder(generateBookingOrderId(listBookingOrder), customers, orderedItemServices, paymentMethod, totalServicePrice);
                listBookingOrder.add(newBookingOrder);
            }

        } else {
            validateCashAmount(totalServicePrice, input);
            newBookingOrder = new BookingOrder(generateBookingOrderId(listBookingOrder), customers, orderedItemServices, paymentMethod, totalServicePrice);
            listBookingOrder.add(newBookingOrder);

        }

        System.out.println("Berhasil menambahkan booking order!");
        System.out.println("Total harga normal service: " + PrintService.formatCurrency(totalServicePrice));
        System.out.println("Total harga yang telah dibayar: " + PrintService.formatCurrency(newBookingOrder.getTotalPayment()));
    }


    private static String generateBookingOrderId(List<BookingOrder> listBookingOrders){
        String result = "Ord-";

        if(listBookingOrders.size() == 0){
            result = "Ord-1";
            return result;
        }

        BookingOrder lastAddedBookingOrder = listBookingOrders.get(listBookingOrders.size() -1);
        int numberOfLastAddedBookingOrder = Integer.parseInt(lastAddedBookingOrder.getBookingId().substring(4));
        int newNumber = numberOfLastAddedBookingOrder + 1;

        result+=newNumber;

        return result;
    }
    private static void validateCashAmount(double totalService, Scanner input){
        System.out.println("Total yang harus dibayarkan dengan uang cash adalah: " + PrintService.formatCurrency(totalService));
        while(true){
            double inputCash = (double) Validation.validasiNumberwithoutRange(input, "^[1-9][0-9]*$", "Hanya bisa menerima input berupa angka di atas nol", "Masukkan jumlah cash yang dibayarkan: " );

            if(inputCash == totalService){
                break;
            } else if (inputCash > totalService) {
                double kembalian = inputCash - totalService;
                System.out.println("Kembalian dari pembayaran service anda : " + PrintService.formatCurrency(kembalian));
                break;
            } else {
                System.out.println("Jumlah uang yang dimasukkan harus sesuai dengan total biaya services.");
            }

        }
    }
    public static void topUpSaldoCoin(Scanner input, List<Customer> customerList) {
        Customer customer = Validation.getMemberById(customerList,authenticatedCustomer.getCustomerId());
        if(customer instanceof MemberCustomer) {
            double jumlahCoin = Validation.validasiNumberwithoutRange(input, "^[1-9][0-9]*$", "Hanya bisa menerima input berupa angka di atas 0", "Masukkan jumlah coin yang akan di top-up: ");
            ((MemberCustomer) customer).setSaldoCoin(jumlahCoin + ((MemberCustomer) customer).getSaldoCoin());

            System.out.println("Top Up Saldo Coin Berhasil!");
        }else {
            System.out.println("Maaf fitur ini hanya untuk Member saja!");
        }

    }

}
