package com.bengkel.booking.services;

import com.bengkel.booking.models.*;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class Validation {
	
	public static String validasiInput(String question, String errorMessage, String regex) {
	    Scanner input = new Scanner(System.in);
	    String result;
	    boolean isLooping = true;
	    do {
	      System.out.print(question);
	      result = input.nextLine();

	      //validasi menggunakan matches
	      if (result.matches(regex)) {
	        isLooping = false;
	      }else {
	        System.out.println(errorMessage);
	      }

	    } while (isLooping);

	    return result;
	}
	
	public static int validasiNumberWithRange(String question, String errorMessage, String regex, int max, int min) {
	    int result;
	    boolean isLooping = true;
	    do {
	      result = Integer.valueOf(validasiInput(question, errorMessage, regex));
	      if (result >= min && result <= max) {
	        isLooping = false;
	      }else {
	        System.out.println("Pilihan angka " + min + " s.d " + max);
	      }
	    } while (isLooping);

	    return result;
	}
	public static int validasiNumberwithoutRange(Scanner input, String regex, String errMessage, String question){
		int validatedNumber;

		validatedNumber = Integer.parseInt(validasiInput(question, errMessage, regex));

		return validatedNumber;
	}

	public static Customer authenticateID(List<Customer> customerList, String id){
		return customerList.stream()
				.filter(customer -> customer.getCustomerId().equals(id))
				.findFirst().orElseThrow();
	}
	public static boolean checkPassword(Customer customerAcc, String password){
		boolean isMatch = false;
		if(customerAcc.getPassword().equals(password)){
			isMatch = true;
			return isMatch;
		}
		return isMatch;
	}

	public static Optional<Customer> getCustumerById(List<Customer> customerList, String id){
		return customerList.stream()
				.filter(customer -> customer.getCustomerId().equals(id))
				.findFirst();
	}
	public static MemberCustomer getMemberById(List<Customer> customerList, String id) {
		return customerList.stream()
				.filter(customer -> customer instanceof MemberCustomer)
				.map(customer -> (MemberCustomer) customer)
				.filter(member -> member.getCustomerId().equalsIgnoreCase(id))
				.findFirst()
				.orElse(null);
	}
	public static Customer validateCustomerId(String idCustomer, List<Customer> customerList){
		return (Customer) customerList.stream()
				.filter(Customer -> Customer.getCustomerId().equalsIgnoreCase(idCustomer))
				.findFirst().orElseThrow();
	}
	public static List<Vehicle> getVehiclesByCustomerId(List<Customer> customerList, String customerId) {
		Optional<Customer> customer = customerList.stream()
				.filter(c -> c.getCustomerId().equals(customerId))
				.findFirst();

		return customer.isPresent() ? customer.get().getVehicles() : null;
	}
	public static Vehicle getVehicleByVehicleId(Customer authenticatedCustomer, String vehicleId) {
		return authenticatedCustomer.getVehicles().stream()
				.filter(vehicle -> vehicle.getVehiclesId().equals(vehicleId))
				.findFirst()
				.orElseThrow(() -> new RuntimeException("Vehicle not found with ID: " + vehicleId));
	}

	public static boolean isMember (List<Customer> customers, String id){
		return customers.stream()
				.filter(customer -> customer instanceof MemberCustomer)
				.anyMatch(MemberCustomer -> MemberCustomer.getCustomerId().equals(id));
	}


	public static boolean checkAddedItemService(List<ItemService> orderedItemServices, String itemServiceId){

		return orderedItemServices.stream()
				.anyMatch(item -> item.getServiceId().equalsIgnoreCase(itemServiceId));
	}

	public static ItemService findItemServiceByItemService(List<ItemService> listItemService, String itemServiceId){

		return listItemService.stream()
				.filter(itemService -> itemService.getServiceId().equalsIgnoreCase(itemServiceId))
				.findFirst()
				.orElseThrow();
	}
}
