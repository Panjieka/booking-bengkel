package com.bengkel.booking.services;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Optional;

import com.bengkel.booking.models.*;

public class PrintService {
	
	public static void printMenu(String[] listMenu, String title) {
		String line = "+---------------------------------+";
		int number = 1;
		String formatTable = " %-2s. %-25s %n";
		
		System.out.printf("%-25s %n", title);
		System.out.println(line);
		
		for (String data : listMenu) {
			if (number < listMenu.length) {
				System.out.printf(formatTable, number, data);
			}else {
				System.out.printf(formatTable, 0, data);
			}
			number++;
		}
		System.out.println(line);
		System.out.println();
	}
	
	public static void printVechicle(List<Vehicle> listVehicle) {
		String formatTable = "| %-2s | %-15s | %-10s | %-15s | %-15s | %-5s | %-15s |%n";
		String line = "+----+-----------------+------------+-----------------+-----------------+-------+-----------------+%n";
		System.out.format(line);
	    System.out.format(formatTable, "No", "Vechicle Id", "Warna", "Brand", "Transmisi", "Tahun", "Tipe Kendaraan");
	    System.out.format(line);
	    int number = 1;
	    String vehicleType = "";
	    for (Vehicle vehicle : listVehicle) {
	    	if (vehicle instanceof Car) {
				vehicleType = "Mobil";
			}else {
				vehicleType = "Motor";
			}
	    	System.out.format(formatTable, number, vehicle.getVehiclesId(), vehicle.getColor(), vehicle.getBrand(), vehicle.getTransmisionType(), vehicle.getYearRelease(), vehicleType);
	    	number++;
	    }
	    System.out.printf(line);
	}
	
	public static void printCustomer(List<Customer> customerList, String id){
		if(Validation.isMember(customerList,id)){
			MemberCustomer memberCustomer = Validation.getMemberById(customerList,id);
			System.out.println();
			System.out.println("MEMBER");
			System.out.println();
			System.out.println("           Customer Profile");
			System.out.println("Customer Id     : " + memberCustomer.getCustomerId());
			System.out.println("Nama            : " + memberCustomer.getName());
			System.out.println("Customer Status : Member");
			System.out.println("Alamat          : " + memberCustomer.getAddress());
			System.out.println("Saldo Koin      : " + formatCurrency(memberCustomer.getSaldoCoin()));
		}else {
			Optional<Customer> customer = Validation.getCustumerById(customerList, id);
			System.out.println();
			System.out.println("NON MEMBER");
			System.out.println();
			System.out.println("           Customer Profile");
			System.out.println("Customer Id     : " + customer.get().getCustomerId());
			System.out.println("Nama            : " + customer.get().getName());
			System.out.println("Customer Status : Non-Member");
			System.out.println("Alamat          : " + customer.get().getAddress());
		}

	}
	public static void PrintItemServices(List<ItemService> listItemServices, Vehicle vehicle){

		System.out.println("List service yang tersedia.");
		String formatTable = "| %-2s | %-15s | %-20s | %-20s | %-15s |%n";
		String line = "+----+-----------------+----------------------+----------------------+-----------------+%n";
		System.out.format(line);
		System.out.format(formatTable, "No", "Service Id", "Nama", "Tipe kendaraan", "Harga");
		System.out.format(line);
		int number = 1;

		List<ItemService> displayedItemServices = listItemServices.stream()
				.filter(service -> service.getVehicleType() == vehicle.getVehicleType())
				.toList();
		for (ItemService item: displayedItemServices) {
			System.out.format(formatTable, number, item.getServiceId(), item.getServiceName(), item.getVehicleType(), formatCurrency(item.getPrice()));
			number++;
		}
		System.out.printf(line);
	}
	public static void printAllBookingOrders(List<BookingOrder> listBookingOrders, Optional<Customer> customer){
		String formatTable = "| %-2s | %-10s | %-10s | %-12s | %-15s | %-15s | %-15s | %-30s |%n";
		String line = "--------------------------------------------------------------------------------------------------------------------------------------%n";
		System.out.format(line);
		System.out.format(formatTable, "No", "Booking Id", "Nama", "Booking date","Payment method", "total price", "total payment", "services");
		System.out.format(line);
		int number = 1;
		List<BookingOrder> filteredBookingOrders = listBookingOrders.stream()
				.filter(bookingOrder -> bookingOrder.getCustomer().getCustomerId().equalsIgnoreCase(customer.get().getCustomerId()))
				.toList();
		for (BookingOrder item : filteredBookingOrders) {
			System.out.format(formatTable, number, item.getBookingId(), item.getCustomer().getName(),item.getBookingDate(), item.getPaymentMethod(), formatCurrency(item.getTotalServicePrice()), formatCurrency(item.getTotalPayment()), printServices(item.getServices()));
			number++;
		}
		System.out.printf(line);
	}
	public static String printServices(List<ItemService> serviceList){
		String result = "";
		for (ItemService service : serviceList) {
			result += service.getServiceName() + ", ";
		}

		String output = result.substring(0, result.length() -2);
		return output;
	}

	public static String formatCurrency (double currency){
		DecimalFormat formatter = new DecimalFormat("#,###.###");
		return formatter.format(currency);
	}
	
}
