package com.bengkel.booking.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.repositories.CustomerRepository;
import com.bengkel.booking.repositories.ItemServiceRepository;

public class MenuService {
	private static List<Customer> listAllCustomers = CustomerRepository.getAllCustomer();
	private static List<ItemService> listAllItemService = ItemServiceRepository.getAllItemService();
	private static Scanner input = new Scanner(System.in);
	private static List<BookingOrder> listBookingOrder = new ArrayList<>();
	
	public static void login() {
		String[] listLogin = {"Login", "Exit"};
		int loginChoice = 0;
		boolean isLooping = true;
		do {
			PrintService.printMenu(listLogin, "Start Menu");
			loginChoice = Validation.validasiNumberWithRange("Masukkan Pilihan: ", "Input Harus Berupa Angka!","^[0-9]+$", listLogin.length-1, 0);
			System.out.println(loginChoice);

			switch (loginChoice){
				case 1:
					if (BengkelService.loginAuth(listAllCustomers,input)){
						mainMenu();
					} else {
						System.out.println("Exit ... ");
						isLooping = false;
					}
					break;
				case 0:
					System.out.println("Exit ... ");
					isLooping = false;
					break;
				default:
					System.out.println("Input tidak valid, silakan coba lagi.");
					break;
			}
		} while (isLooping);
	}
	
	public static void mainMenu() {
		String[] listMenu = {"Informasi Customer", "Booking Bengkel", "Top Up Bengkel Coin", "Informasi Booking", "Logout"};
		int menuChoice = 0;
		boolean isLooping = true;
		
		do {
			System.out.println();
			PrintService.printMenu(listMenu, "Booking Bengkel Menu");
			menuChoice = Validation.validasiNumberWithRange("Masukan Pilihan Menu:", "Input Harus Berupa Angka!", "^[0-9]+$", listMenu.length-1, 0);
			System.out.println(menuChoice);
			
			switch (menuChoice) {
			case 1:
				//panggil fitur Informasi Customer
				BengkelService.showInfoCustomer(listAllCustomers);
				BengkelService.showVehicleCustomer(listAllCustomers);
				break;
			case 2:
				BengkelService.createReservation(input, listAllCustomers, listAllItemService, listBookingOrder);
				break;
			case 3:
				BengkelService.topUpSaldoCoin(input,listAllCustomers);
				break;
			case 4:
				BengkelService.showBookingOrder(listBookingOrder,listAllCustomers);
				break;
			default:
				System.out.println("Logout");
				isLooping = false;
				break;
			}
		} while (isLooping);
		
		
	}
	
	//Silahkan tambahkan kodingan untuk keperluan Menu Aplikasi
}
